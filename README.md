# rude

MUD game, in rust, so that hopefully it goes fast and doesn't break things.

Under heavy development. Much is currently unimplemented or not completely
implemented yet.

API docs are available at
[https://rascul.gitlab.io/rude/rude/index.html](https://rascul.gitlab.io/rude/rude/index.html).

```
git clone https://gitlab.com/rascul/rude
cd rude
sqlite3 rude.db < schema.sql
cp config.toml.default config.toml
cargo run
```
