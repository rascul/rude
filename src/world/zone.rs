use std::cmp::PartialEq;
use std::collections::HashSet;
use std::convert::TryFrom;
use std::error::Error;

use rusqlite::Row;
use serde_derive::{Deserialize, Serialize};

use crate::id::Id;
use crate::try_log;
use crate::world::{Area, AreaType};

/// A collection of rooms and/or other zones.
#[derive(Debug, Deserialize, Serialize)]
pub struct Zone {
	/// Unique identifier for the zone
	pub id: Id,

	/// Identifier of the parent zone
	pub parent: Id,

	/// Name of the zone
	pub name: String,

	/// Whether players in the zone should be visible on where to the parent
	pub users_visible: bool,

	/// Areas contained inside this zone
	pub areas: HashSet<Id>,
}

impl From<&Zone> for Zone {
	fn from(zone: &Zone) -> Self {
		Self {
			id: zone.id,
			parent: zone.parent,
			name: zone.name.clone(),
			users_visible: zone.users_visible,
			areas: zone.areas.clone(),
		}
	}
}

impl PartialEq for Zone {
	fn eq(&self, other: &Self) -> bool {
		self.id == other.id
			&& self.parent == other.parent
			&& self.name == other.name
			&& self.users_visible == other.users_visible
			&& self.areas == other.areas
	}
}

impl Area for Zone {
	fn id(&self) -> Id {
		self.id
	}

	fn parent(&self) -> Id {
		self.parent
	}

	fn name(&self) -> String {
		self.name.to_owned()
	}

	fn visible(&self) -> bool {
		self.users_visible
	}

	fn area_type(&self) -> AreaType {
		AreaType::Zone
	}
}

impl<'a> TryFrom<&Row<'a>> for Zone {
	type Error = Box<dyn Error>;

	fn try_from(row: &Row) -> Result<Self, Self::Error> {
		Ok(Self {
			id: try_log!(row.get("id"), "Unable to get id from row"),
			parent: try_log!(row.get("parent"), "Unable to get parent from row"),
			name: try_log!(row.get("name"), "Unable to get name from row"),
			users_visible: try_log!(
				row.get("users_visible"),
				"Unable to get users_visible from row"
			),
			areas: HashSet::new(),
		})
	}
}
