use crate::id::Id;

/// The type of the `Area`.
#[derive(Debug)]
pub enum AreaType {
	Room,
	Zone,
}

/// An 'Area' is an identifiable location, either a specific `Room` or maybe a
/// grouping of `Area`s.
pub trait Area: std::fmt::Debug {
	/// Returns the unique identifier of the `Area`.
	fn id(&self) -> Id;

	/// Return the unique identifier of the parent `Area`.
	fn parent(&self) -> Id;

	/// Text name of the `Area`.
	fn name(&self) -> String;

	/// Specifies whether users in this `Area` will be visibile (appearing in
	/// the area `self.name()`) to the parent `Area` and its children. This
	/// allows for granularity over the where command.
	fn visible(&self) -> bool;

	/// The type of area, either it's a `Room` or a `Zone`.
	fn area_type(&self) -> AreaType;

	/// Returns true if this `Area` is the root, or the world. This is
	/// determined to be the case if `self.id()` and `self.parent()` return
	/// the same `Uuid`.
	fn is_world(&self) -> bool {
		self.id() == self.parent()
	}
}
