use std::convert::TryFrom;
use std::error::Error;

use rusqlite::Row;
use serde_derive::{Deserialize, Serialize};

use crate::id::Id;
use crate::world::Direction;

/// A one way exit, from one room to another, with a direction.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Exit {
	/// Target room ID
	pub target: Id,

	/// Exit direction
	pub direction: Direction,
}

impl<'a> TryFrom<&Row<'a>> for Exit {
	type Error = Box<dyn Error>;

	fn try_from(row: &Row) -> Result<Self, Self::Error> {
		Ok(Self {
			target: try_log!(row.get("target"), "Unable to get column 'target' from row"),
			direction: try_log!(
				Direction::try_from_long(try_log!(
					row.get::<&str, String>("direction"),
					"Unable to get column 'direction' from row"
				)),
				"Unable to parse direction"
			),
		})
	}
}
