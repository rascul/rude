//! The game world.
//!
//! The world is divided up into zones and rooms. A zone can contain a
//! collection of both rooms and zones. Each zone and room can specify if the
//! players inside it are visible to the sibling rooms and zones.
//!
//! This allows for some granularity in the zone design. An example, there may
//! be a zone for a city, containing a few rooms for gates and a few zones for
//! different regions of the city. One might be the trade district, and each
//! shop might be a single room, or zone with a few rooms depicting the layout
//! of a large store.

mod area;
mod direction;
mod exit;
mod room;
mod zone;

pub use area::*;
pub use direction::*;
pub use exit::Exit;
pub use room::Room;
pub use zone::Zone;
