//! Mud commands
//!
//! This module contains the command parser and the commands themselves. A
//! generic interface is provided to make it easier to parse the input, map
//! it to a function, call that function, and provide help on commands.
//!
//! The command parsing is done in such a fashion that only enough characters
//! to unambiguously desginate a command are needed. That means "lo" will
//! match "look" as long as there's not a "log". If multiple matches are
//! found, the first one in alphabetical order is selected. This action may
//! change in the future.
//!
//! The help system isn't implemented yet.
//!
//! Each command is to be listed in the `Command` enum, which is the root
//! level of the commands. It and any subcommands must implement the `Parse`
//! trait, and if there are subcommands then certain trait methods must be
//! implemented for the subcommand parsing and matching to function properly.

mod command;
mod dispatch;
mod new;
mod parse;
mod parser_error;
mod set;

pub use command::Command;
pub use new::*;
pub use parse::Parse;
pub use parser_error::ParserError;
pub use set::*;
