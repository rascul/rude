use mio::Token;

use crate::command::{Command, Parse};
use crate::database::Db;
use crate::queue::SendQueue;

impl Command {
	pub fn dispatch_help(command: &Command, args: String, token: Token, _db: &mut Db) -> SendQueue {
		if args.is_empty() {
			return (token, command.commands()).into();
		}

		let (new_command, new_args) = match Command::parse(args) {
			Ok((help_command, args)) => (help_command, args),
			Err(e) => return (token, e.to_string()).into(),
		};

		if let Some(sub_help) = new_command.subcommand_help() {
			// woo
		} else {
			return (token, new_command.commands()).into();
		}

		// let mut command = command;
		// let mut args = args;

		// while !args.is_empty() {
		// 	let (new_command, new_args) = match Command::parse(args) {
		// 		Ok((help_command, args)) => (help_command, args),
		// 		Err(e) => return (token, e.to_string()).into(),
		// 	};
		// }

		// if let Some(sub_help) = help_command.subcommand_help() {
		// 	return (token, sub_help).into();
		// }

		SendQueue::ok(token)
	}
}

fn recurse_help(command: &Command, args: String) -> String {
	if args.is_empty() {
		return command.commands();
	}

	"".into()
}
