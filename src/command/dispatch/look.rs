use colored::Colorize;
use mio::Token;

use crate::command::Command;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl Command {
	/// Look at the room. Provide room name, description, and exits.
	pub fn dispatch_look(_: &Command, _: String, token: Token, db: &mut Db) -> SendQueue {
		let mut send_queue = SendQueue::new();

		// get the player
		let player = try_option_send_error!(token, db.get_connected_player(token));

		// get the room
		let room = try_option_send_error!(token, db.load_room(player.location));

		// room name
		send_queue.push(
			token,
			format!("{}\n", room.name.cyan().to_string()),
			false,
			None,
		);

		// room description
		send_queue.push(
			token,
			format!("{}\n", {
				let mut s = room.description.join("\n");
				s.push_str("\n");
				s
			}),
			false,
			None,
		);

		// exits
		send_queue.push(
			token,
			format!("[ obvious exits: {} ]\n", room.exit_string())
				.cyan()
				.to_string(),
			false,
			None,
		);

		// other people in room
		for (neighbor_token, neighbor_player) in try_send_error!(
			token,
			db.find_connected_players_by_location(player.location)
		) {
			if neighbor_token != token {
				send_queue.push(
					token,
					format!("{} is here", neighbor_player.name),
					false,
					None,
				);
			}
		}

		send_queue.push(token, "", true, None);

		send_queue
	}
}
