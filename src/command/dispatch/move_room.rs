use log::warn;
use mio::Token;

use crate::command::Command;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::world::*;
use crate::{try_option_send_error, try_send_error};

impl Command {
	pub fn dispatch_move_room(
		command: &Command,
		_args: String,
		token: Token,
		db: &mut Db,
	) -> SendQueue {
		let mut send_queue = SendQueue::new();

		let direction: Direction = match command {
			Command::N | Command::North => Direction::North,
			Command::S | Command::South => Direction::South,
			Command::E | Command::East => Direction::East,
			Command::W | Command::West => Direction::West,
			Command::U | Command::Up => Direction::Up,
			Command::D | Command::Down => Direction::Down,
			_ => {
				warn!("Can't figure out direction: {:?}", command);
				return send_queue;
			}
		};

		// find the player
		let mut player = try_option_send_error!(token, db.get_connected_player(token));

		// get starting room
		let start_room = try_option_send_error!(token, db.load_room(player.location));

		// get the exit
		let exit = if let Some(exit) = start_room.exits.get(&direction) {
			exit
		} else {
			send_queue.push(token, "You can't go that way.", true, None);
			return send_queue;
		};

		// get the target room
		let target_room = try_option_send_error!(token, db.load_room(exit.target));

		// move and save the player
		player.location = target_room.id;
		let _ = try_send_error!(token, db.save_player(&player));
		let _ = try_send_error!(token, db.save_connected_player(token, &player));
		send_queue.push(
			token,
			format!("You leave {}.\n\n", direction.long()),
			false,
			None,
		);

		// tell people about leaving
		for (neighbor_token, _) in
			try_send_error!(token, db.find_connected_players_by_location(start_room.id))
		{
			if neighbor_token != token {
				send_queue.push(
					neighbor_token,
					format!("{} leaves {}.", player.name, direction.long()),
					true,
					None,
				);
			}
		}

		// tell people about entering
		for (neighbor_token, _) in
			try_send_error!(token, db.find_connected_players_by_location(target_room.id))
		{
			if neighbor_token != token {
				send_queue.push(
					neighbor_token,
					format!(
						"{} arrives from the {}.",
						player.name,
						direction.opposite().long()
					),
					true,
					None,
				);
			}
		}

		// look around
		send_queue.append(&mut Self::dispatch_look(&command, String::new(), token, db));

		send_queue
	}
}
