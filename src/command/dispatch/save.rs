use mio::Token;

use crate::command::Command;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl Command {
	/// Save the player information to disk.
	pub fn dispatch_save(_: &Command, _: String, token: Token, db: &mut Db) -> SendQueue {
		let mut send_queue = SendQueue::new();

		let player = try_option_send_error!(token, db.get_connected_player(token));
		let _ = try_send_error!(token, db.save_player(&player));
		let _ = try_send_error!(token, db.save_connected_player(token, &player));

		send_queue.push(token, "Ok", true, None);
		send_queue
	}
}
