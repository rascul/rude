use mio::Token;

use crate::command::Command;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl Command {
	/// Say something to anyone in the room.
	pub fn dispatch_say(_: &Command, args: String, token: Token, db: &mut Db) -> SendQueue {
		let mut send_queue = SendQueue::new();

		let player = try_option_send_error!(token, db.get_connected_player(token));

		for (neighbor_token, _) in try_send_error!(
			token,
			db.find_connected_players_by_location(player.location)
		) {
			send_queue.push(
				neighbor_token,
				if neighbor_token == token {
					["You say, \"", &args, "\"\n"].concat()
				} else {
					[&player.name, " says, \"", &args, "\"\n"].concat()
				},
				true,
				None,
			);
		}

		send_queue
	}
}
