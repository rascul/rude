use mio::Token;

use crate::command::Command;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::state::State;

impl Command {
	/// Quit the game
	pub fn dispatch_quit(&self, _: String, token: Token, _: &mut Db) -> SendQueue {
		SendQueue(vec![(token, "Goodbye\n\n".into(), false, Some(State::Quit))].into())
	}
}
