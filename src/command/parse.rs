use std::string::ToString;

use mio::Token;
use strum::IntoEnumIterator;

use crate::command::ParserError;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::result::RudeResult;

/// Command parser.
///
/// Commands consist of the text from the beginning of a string, up to but not
/// including the first space character or newline. If a space, that space is removed and the
/// rest of the string is put into args. If a newline, args will be empty. The command part of the
/// string is then parsed for a match with the variants of the enum. After that, control is passed
/// to `parse_subcommand()` to parse a subcommand if necessary. Also integrated into this is the
/// help system.
pub trait Parse: Clone + Default + IntoEnumIterator + PartialEq + ToString {
	/// Help text for a single command.
	fn help(&self) -> &str;

	/// Check if there's a subcommand. The default implementation does not support subcommands.
	/// If subcommands are necessary, you must implement the `subcommand()` function.
	fn parse_subcommand(&self, s: String) -> RudeResult<(Self, String)> {
		Ok((self.clone(), s))
	}

	/// Gets the `help_all()` for a subcommand. The default implementation does not support
	/// subcommands. You must implement the `subcommand_help()` function if you want sumcommands.
	fn subcommand_help(&self) -> Option<String> {
		None
	}

	/// Map of variants to functions to be called with 'dispatch()'.
	fn dispatch_map(&self) -> fn(&Self, String, Token, &mut Db) -> SendQueue;

	/// Must be implemented if there are subcommands.
	fn dispatch_map_subcommand(&self, _: String, _: Token, _: &mut Db) -> SendQueue {
		SendQueue::new()
	}

	/// This is to call the function associated with a command.
	fn dispatch(
		&self,
		command: &Self,
		command_text: String,
		token: Token,
		db: &mut Db,
	) -> SendQueue {
		let function = self.dispatch_map();
		function(&command, command_text, token, db)
	}

	/// Boilerplate for default variant. Shouldn't ever be called. Probably should log if it is.
	fn dispatch_default(&self, _: String, _: Token, _: &mut Db) -> SendQueue {
		SendQueue::new()
	}

	/// Help text for all commands
	fn help_all(&self) -> String {
		Self::iter()
			.filter_map(|a| {
				if a == Self::default() {
					None
				} else {
					Some(a.help().into())
				}
			})
			.collect::<Vec<String>>()
			.join("\n")
	}

	/// List of all commands
	fn commands(&self) -> String {
		Self::iter()
			.filter_map(|a| {
				if a == Self::default() {
					None
				} else {
					Some(a.to_string().to_lowercase())
				}
			})
			.collect::<Vec<String>>()
			.join(" ")
	}

	/// Parse a `Into<String>` into a command.
	fn parse<S: Into<String>>(s: S) -> RudeResult<(Self, String)> {
		let mut s = s.into();

		// get the text command and the args
		let mut args: String = if let Some(pos) = s.find(' ') {
			s.split_off(pos)
		} else {
			String::new()
		};

		if args.starts_with(' ') {
			args = args.split_off(1);
		}

		// no command
		if s.is_empty() {
			return Err(ParserError::Empty.into());
		}

		let mut matches: Vec<Self> = Vec::new();

		// look for matches
		for action in Self::iter() {
			if s == action.to_string() {
				matches = vec![action];
				break;
			} else if action.to_string().to_lowercase().starts_with(s.as_str()) {
				matches.push(action);
			}
		}

		// check if there was a match
		if matches.is_empty() {
			return Err(ParserError::Unknown.into());
		}

		// sort so the first match is the best
		matches.sort_by(|a, b| a.to_string().cmp(&b.to_string()));

		// default is an error
		if matches[0] == Self::default() {
			Err(ParserError::Default.into())
		} else {
			Ok(matches[0].parse_subcommand(args)?)
		}
	}
}
