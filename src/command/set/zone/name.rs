use mio::Token;

use crate::command::CommandSetZone;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl CommandSetZone {
	/// Change the zone name
	pub fn dispatch_name(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		let player = try_option_send_error!(token, db.get_connected_player(token));

		let mut zone = try_option_send_error!(token, db.load_zone(player.location));
		zone.name = args;

		let _ = try_send_error!(token, db.save_zone(&zone));

		SendQueue::ok(token)
	}
}
