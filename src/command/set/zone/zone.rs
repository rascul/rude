use std::default::Default;

use mio::Token;
use strum_macros::{Display, EnumIter};

use crate::command::Parse;
use crate::database::Db;
use crate::queue::SendQueue;

/// Change zone properties
#[derive(Clone, Debug, Display, EnumIter, Eq, Ord, PartialEq, PartialOrd)]
pub enum CommandSetZone {
	/// Change the name of the zone
	Name,

	Default,
}

impl Default for CommandSetZone {
	fn default() -> Self {
		Self::Default
	}
}

impl Parse for CommandSetZone {
	fn help(&self) -> &str {
		match self {
			Self::Name => "set zone name NEW_NAME :: Set zone name to NEW_NAME.",
			Self::Default => "",
		}
	}

	fn dispatch_map(&self) -> fn(&Self, String, Token, &mut Db) -> SendQueue {
		match self {
			Self::Name => Self::dispatch_name,
			Self::Default => Self::dispatch_default,
		}
	}
}
