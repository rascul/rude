use std::default::Default;

use mio::Token;
use strum_macros::{Display, EnumIter};

use crate::command::{CommandSetPlayer, CommandSetRoom, CommandSetZone, Parse, ParserError};
use crate::database::Db;
use crate::queue::SendQueue;
use crate::result::RudeResult;
use crate::try_log;

/// Set various options
#[derive(Clone, Debug, Display, EnumIter, Eq, Ord, PartialEq, PartialOrd)]
pub enum CommandSet {
	/// Player options
	Player(CommandSetPlayer),

	/// Room options
	Room(CommandSetRoom),

	/// Zone options
	Zone(CommandSetZone),

	Default,
}

impl Default for CommandSet {
	fn default() -> Self {
		Self::Default
	}
}

impl Parse for CommandSet {
	fn help(&self) -> &str {
		match self {
			Self::Player(_) => "set player :: Set player options.",
			Self::Room(_) => "set room :: Set room options.",
			Self::Zone(_) => "set zone :: Set zone options.",
			Self::Default => "",
		}
	}

	fn parse_subcommand(&self, s: String) -> RudeResult<(Self, String)> {
		match self {
			Self::Player(_) => {
				let (command, args) = try_log!(
					CommandSetPlayer::parse(s),
					"Unable to parse player subcommand",
				);
				Ok((Self::Player(command), args))
			}
			Self::Room(_) => {
				let (command, args) =
					try_log!(CommandSetRoom::parse(s), "Unable to parse room subcommand",);
				Ok((Self::Room(command), args))
			}
			Self::Zone(_) => {
				let (command, args) =
					try_log!(CommandSetZone::parse(s), "Unable to parse zone subcommand",);
				Ok((Self::Zone(command), args))
			}
			Self::Default => Err(ParserError::Default.into()),
		}
	}

	fn dispatch_map_subcommand(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		match self {
			Self::Player(command_set_player) => {
				command_set_player.dispatch(command_set_player, args, token, db)
			}
			Self::Room(command_set_room) => {
				command_set_room.dispatch(command_set_room, args, token, db)
			}
			Self::Zone(command_set_zone) => {
				command_set_zone.dispatch(command_set_zone, args, token, db)
			}
			_ => SendQueue::new(),
		}
	}

	fn dispatch_map(&self) -> fn(&Self, String, Token, &mut Db) -> SendQueue {
		match self {
			Self::Player(_) => Self::dispatch_map_subcommand,
			Self::Room(_) => Self::dispatch_map_subcommand,
			Self::Zone(_) => Self::dispatch_map_subcommand,
			Self::Default => Self::dispatch_default,
		}
	}
}
