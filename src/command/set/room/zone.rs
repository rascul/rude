use mio::Token;

use crate::command::CommandSetRoom;
use crate::database::Db;
use crate::id::Id;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl CommandSetRoom {
	/// Change the room's containing zone
	pub fn dispatch_zone(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		let player = try_option_send_error!(token, db.get_connected_player(token));
		let mut room = try_option_send_error!(token, db.load_room(player.location));
		let mut start_zone = try_option_send_error!(token, db.load_zone(room.id));
		let mut end_zone = try_option_send_error!(
			token,
			db.load_zone(try_send_error!(token, Id::parse_str(&args)))
		);

		if start_zone == end_zone {
			return (token, "Room is already in that zone").into();
		}

		room.zone = end_zone.id;
		start_zone.areas.remove(&room.id);
		end_zone.areas.insert(room.id);

		let _ = try_send_error!(token, db.save_room(&room));
		let _ = try_send_error!(token, db.save_zone(&start_zone));
		let _ = try_send_error!(token, db.save_zone(&end_zone));

		SendQueue::ok(token)
	}
}
