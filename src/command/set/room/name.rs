use mio::Token;

use crate::command::CommandSetRoom;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl CommandSetRoom {
	/// Change the room name
	pub fn dispatch_name(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		let player = try_option_send_error!(token, db.get_connected_player(token));

		let mut room = try_option_send_error!(token, db.load_room(player.location));
		room.name = args;

		let _ = try_send_error!(token, db.save_room(&room));

		SendQueue::ok(token)
	}
}
