use std::default::Default;

use mio::Token;
use strum_macros::{Display, EnumIter};

use crate::command::{CommandSetRoomDescription, Parse, ParserError};
use crate::database::Db;
use crate::queue::SendQueue;
use crate::result::RudeResult;
use crate::try_log;

/// Set room properties
#[derive(Clone, Debug, Display, EnumIter, Eq, Ord, PartialEq, PartialOrd)]
pub enum CommandSetRoom {
	/// Change the room description
	Description(CommandSetRoomDescription),

	/// Set the room name
	Name,

	/// Set the parent zone
	Zone,

	Default,
}

impl Default for CommandSetRoom {
	fn default() -> Self {
		Self::Default
	}
}

impl Parse for CommandSetRoom {
	fn help(&self) -> &str {
		match self {
			Self::Description(_) => "",
			Self::Name => "set room name NEW_NAME :: Set room name to NEW_NAME.",
			Self::Zone => "set room zone ZONE_ID :: Set room zone to ZONE_ID.",
			Self::Default => "",
		}
	}

	fn parse_subcommand(&self, s: String) -> RudeResult<(Self, String)> {
		match self {
			Self::Description(_) => {
				let (command, args) = try_log!(
					CommandSetRoomDescription::parse(s),
					"Unable to parse room description command"
				);
				Ok((Self::Description(command), args))
			}
			Self::Default => Err(ParserError::Default.into()),
			_ => Ok((self.clone(), s)),
		}
	}

	fn dispatch_map_subcommand(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		match self {
			Self::Description(command) => command.dispatch(command, args, token, db),
			_ => SendQueue::new(),
		}
	}

	fn dispatch_map(&self) -> fn(&Self, String, Token, &mut Db) -> SendQueue {
		match self {
			Self::Description(_) => Self::dispatch_map_subcommand,
			Self::Name => Self::dispatch_name,
			Self::Zone => Self::dispatch_zone,
			Self::Default => Self::dispatch_default,
		}
	}
}
