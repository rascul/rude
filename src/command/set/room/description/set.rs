use std::str::FromStr;

use mio::Token;

use crate::command::CommandSetRoomDescription;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{option_send, try_option_send_error, try_send, try_send_error};

impl CommandSetRoomDescription {
	/// Set the specified line in the description to the specified text.
	pub fn dispatch_set(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		// make sure something was provided
		if args.is_empty() {
			return SendQueue(
				vec![(
					token,
					"Set what line to what description?".into(),
					true,
					None,
				)]
				.into(),
			);
		}

		// parse the arguments
		let space = option_send!(token, args.find(' '), "Set what line to what description?");
		let mut line_string = args.clone();
		let mut description = line_string.split_off(space);
		description.remove(0);
		let line_num: usize = try_send!(
			token,
			u8::from_str(&line_string),
			"Can't figure out line number from '{}'.",
			line_string
		)
		.into();

		// load the player and room
		let player = try_option_send_error!(token, db.get_connected_player(token));
		let mut room = try_option_send_error!(token, db.load_room(player.location));

		// make sure description has enough lines
		if line_num > room.description.len().into() {
			for _ in room.description.len()..line_num {
				room.description.push("".into());
			}
		}

		// set description and save
		room.description[line_num - 1] = description;
		let _ = try_send_error!(token, db.save_room(&room));

		SendQueue::ok(token)
	}
}
