use std::default::Default;

use mio::Token;
use strum_macros::{Display, EnumIter};

use crate::command::Parse;
use crate::database::Db;
use crate::queue::SendQueue;

/// Modify the room description
#[derive(Clone, Debug, Display, EnumIter, Eq, Ord, PartialEq, PartialOrd)]
pub enum CommandSetRoomDescription {
	/// Delete a line
	Delete,

	/// Set a line
	Set,

	Default,
}

impl Default for CommandSetRoomDescription {
	fn default() -> Self {
		Self::Default
	}
}

impl Parse for CommandSetRoomDescription {
	fn help(&self) -> &str {
		match self {
			Self::Delete => {
				"set room description delete LINE :: Delete line LINE from room description"
			}
			Self::Set => {
				"set room description set LINE DESC :: Set room description line LINE to DESC"
			}
			Self::Default => "",
		}
	}

	fn dispatch_map(&self) -> fn(&Self, String, Token, &mut Db) -> SendQueue {
		match self {
			Self::Delete => Self::dispatch_delete,
			Self::Set => Self::dispatch_set,
			Self::Default => Self::dispatch_default,
		}
	}
}
