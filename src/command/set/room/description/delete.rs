use std::str::FromStr;

use mio::Token;

use crate::command::CommandSetRoomDescription;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send, try_send_error};

impl CommandSetRoomDescription {
	/// Delete the specified line from the description.
	pub fn dispatch_delete(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		// make sure something was provided
		if args.is_empty() {
			return (token, "Delete which line?").into();
		}

		// try and get the line number
		let line_num: usize = try_send!(
			token,
			u8::from_str(&args),
			"Can't figure out line number from '{}'.",
			&args
		)
		.into();

		// load the player and room
		let player = try_option_send_error!(token, db.get_connected_player(token));
		let mut room = try_option_send_error!(token, db.load_room(player.location));

		// make sure description has enough lines
		if line_num > room.description.len().into() || line_num <= 0 {
			return (token, "Line doesn't exist").into();
		}

		// remove the line and save
		room.description.remove(line_num - 1);
		let _ = try_send_error!(token, db.save_room(&room));

		SendQueue::ok(token)
	}
}
