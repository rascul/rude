use mio::Token;

use crate::check;
use crate::command::CommandSetPlayer;
use crate::database::Db;
use crate::password::Password;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl CommandSetPlayer {
	/// Set the player's password
	pub fn dispatch_password(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		let player = try_option_send_error!(token, db.get_connected_player(token));

		match check::player_password(args) {
			check::PlayerCheck::Ok(checked_pass) => {
				let password = try_send_error!(token, Password::new(checked_pass));
				let _ = try_send_error!(token, db.save_password(player.id, &password));
				SendQueue::ok(token)
			}
			check::PlayerCheck::Err(err_vec) => {
				let mut s: String = err_vec.join("\n");
				s.push_str("\n");
				(token, s).into()
			}
		}
	}
}
