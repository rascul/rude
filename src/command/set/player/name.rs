use mio::Token;

use crate::command::CommandSetPlayer;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::{try_option_send_error, try_send_error};

impl CommandSetPlayer {
	/// Set the player's name
	pub fn dispatch_name(&self, args: String, token: Token, db: &mut Db) -> SendQueue {
		let mut player = try_option_send_error!(token, db.get_connected_player(token));

		let new_name = args.trim();
		if new_name.is_empty() {
			return (token, "Name can't be empty").into();
		}

		player.name = new_name.to_string();

		let _ = try_send_error!(token, db.save_player(&player));
		let _ = try_send_error!(token, db.save_connected_player(token, &player));

		SendQueue::ok(token)
	}
}
