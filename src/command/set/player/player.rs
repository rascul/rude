use std::default::Default;

use mio::Token;
use strum_macros::{Display, EnumIter};

use crate::command::Parse;
use crate::database::Db;
use crate::queue::SendQueue;

/// Modify player properties
#[derive(Clone, Debug, Display, EnumIter, Eq, Ord, PartialEq, PartialOrd)]
pub enum CommandSetPlayer {
	/// Set the player's name
	Name,

	/// Set the password
	Password,

	Default,
}

impl Default for CommandSetPlayer {
	fn default() -> Self {
		Self::Default
	}
}

impl Parse for CommandSetPlayer {
	fn help(&self) -> &str {
		match self {
			Self::Name => "set player name NEW_NAME :: Set player name to NEW_NAME.",
			Self::Password => "set player password NEW_PASS :: Set password to NEW_PASS",
			Self::Default => "",
		}
	}

	fn dispatch_map(&self) -> fn(&Self, String, Token, &mut Db) -> SendQueue {
		match self {
			Self::Name => Self::dispatch_name,
			Self::Password => Self::dispatch_password,
			Self::Default => Self::dispatch_default,
		}
	}
}
