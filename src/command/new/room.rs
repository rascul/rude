use std::collections::HashMap;

use mio::Token;

use crate::command::CommandNew;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::world::Room;
use crate::{try_option_send_error, try_send_error};

impl CommandNew {
	/// Create a new room
	pub fn dispatch_room(
		_command: &CommandNew,
		_args: String,
		token: Token,
		db: &mut Db,
	) -> SendQueue {
		let player = try_option_send_error!(token, db.get_connected_player(token));
		let room = try_option_send_error!(token, db.load_room(player.location));
		let zone = try_option_send_error!(token, db.load_zone(room.zone));
		let mut root = try_send_error!(token, db.root_zone(&zone));
		let new_room_id = try_send_error!(token, db.new_area_id());

		let new_room = Room {
			id: new_room_id,
			zone: root.id,
			name: format!("New Room {}", new_room_id),
			description: Vec::new(),
			users_visible: true,
			exits: HashMap::new(),
		};

		let _ = try_send_error!(token, db.save_room(&new_room));
		let _ = root.areas.insert(new_room.id);
		let _ = try_send_error!(token, db.save_zone(&root));

		SendQueue(
			vec![(
				token,
				format!("Room created with id {}", new_room_id),
				true,
				None,
			)]
			.into(),
		)
	}
}
