use std::default::Default;

use mio::Token;
use strum_macros::{Display, EnumIter};

use crate::command::Parse;
use crate::database::Db;
use crate::queue::SendQueue;

/// Create new things
#[derive(Clone, Debug, Display, EnumIter, Eq, Ord, PartialEq, PartialOrd)]
pub enum CommandNew {
	/// Create a new room
	Room,

	/// Create a new zone
	Zone,

	Default,
}

impl Default for CommandNew {
	fn default() -> Self {
		Self::Default
	}
}

impl Parse for CommandNew {
	fn help(&self) -> &str {
		match self {
			Self::Room => "new room :: Create a new room.",
			Self::Zone => "new zone :: Create a new zone.",
			Self::Default => "",
		}
	}

	fn dispatch_map(&self) -> fn(&Self, String, Token, &mut Db) -> SendQueue {
		match self {
			Self::Room => Self::dispatch_room,
			Self::Zone => Self::dispatch_zone,
			Self::Default => Self::dispatch_default,
		}
	}
}
