use std::collections::HashSet;

use mio::Token;

use crate::command::CommandNew;
use crate::database::Db;
use crate::queue::SendQueue;
use crate::world::Zone;
use crate::{try_option_send_error, try_send_error};

impl CommandNew {
	/// Create a new zone
	pub fn dispatch_zone(
		_command: &CommandNew,
		_args: String,
		token: Token,
		db: &mut Db,
	) -> SendQueue {
		let player = try_option_send_error!(token, db.get_connected_player(token));
		let room = try_option_send_error!(token, db.load_room(player.location));
		let zone = try_option_send_error!(token, db.load_zone(room.zone));
		let mut root = try_send_error!(token, db.root_zone(&zone));
		let new_zone_id = try_send_error!(token, db.new_area_id());

		let new_zone = Zone {
			id: new_zone_id,
			parent: root.id,
			name: format!("New Zone {}", new_zone_id),
			users_visible: true,
			areas: HashSet::new(),
		};

		let _ = try_send_error!(token, db.save_zone(&new_zone));
		let _ = root.areas.insert(new_zone.id);
		let _ = try_send_error!(token, db.save_zone(&root));

		SendQueue(
			vec![(
				token,
				format!("Zone created with id {}", new_zone_id),
				true,
				None,
			)]
			.into(),
		)
	}
}
