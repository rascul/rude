use std::error::Error;
use std::fmt;

/// Errors that may be emitted by the command parser
#[derive(Debug)]
pub enum ParserError {
	/// No command provided
	Empty,

	/// Unknown command
	Unknown,

	Default,
}

impl fmt::Display for ParserError {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::Empty => fmt::Display::fmt("No command provided", f),
			Self::Unknown => fmt::Display::fmt("Unknown command", f),
			Self::Default => fmt::Display::fmt("Unknown command", f),
		}
	}
}

impl Error for ParserError {}
