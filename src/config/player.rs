use serde_derive::Deserialize;

use crate::id::Id;

/// Player configuration
#[derive(Clone, Debug, Deserialize)]
pub struct Player {
	/// Default starting location
	pub starting_location: Id,

	/// Minimum length of player name
	pub name_min: usize,

	/// Maximum length of player name
	pub name_max: usize,

	/// Allowed characters in player name
	pub name_chars: String,

	/// Minimum length of password
	pub pass_min: usize,

	/// Maximum length of password
	pub pass_max: usize,

	/// Allowed characters in password
	pub pass_chars: String,
}
