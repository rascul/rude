use serde_derive::Deserialize;

/// Server configuration
#[derive(Clone, Debug, Deserialize)]
pub struct Server {
	/// IP address to listen on.
	pub ip: String,

	/// Port to listen on.
	pub port: i64,

	/// Max number of connected players.
	pub connection_limit: usize,

	/// Socket event capacity.
	pub event_capacity: usize,
}
