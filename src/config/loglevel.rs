use log::LevelFilter;
use serde_derive::Deserialize;

/// Helper enum to handle deserialization from toml because `LevelFilter`
/// from the log crate does not.
#[derive(Clone, Debug, Deserialize)]
pub enum LogLevel {
	/// log::LevelFilter::Off
	#[allow(non_camel_case_types)]
	off,

	/// log::LevelFilter::Error
	#[allow(non_camel_case_types)]
	error,

	/// log::LevelFilter::Warn
	#[allow(non_camel_case_types)]
	warn,

	/// log::LevelFilter::Info
	#[allow(non_camel_case_types)]
	info,

	/// log::LevelFilter::Debug
	#[allow(non_camel_case_types)]
	debug,

	/// log::LevelFilter::Trace
	#[allow(non_camel_case_types)]
	trace,
}

impl LogLevel {
	/// Return the associated `LevelFilter`.
	pub fn level(&self) -> LevelFilter {
		match &self {
			Self::off => LevelFilter::Off,
			Self::error => LevelFilter::Error,
			Self::warn => LevelFilter::Warn,
			Self::info => LevelFilter::Info,
			Self::debug => LevelFilter::Debug,
			Self::trace => LevelFilter::Trace,
		}
	}
}
