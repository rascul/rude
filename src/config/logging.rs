use serde_derive::Deserialize;

use crate::config::LogLevel;

/// Logging configuration
#[derive(Clone, Debug, Deserialize)]
pub struct Logging {
	/// Level of log verbosity.
	pub level: LogLevel,
}
