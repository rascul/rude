//! State information for a connected `Client`.

use crate::password::Password;

/// Play state for a conected `Client`.
#[derive(Clone, Debug, PartialEq)]
pub enum State {
	/// Logging in
	Login(Login),

	/// Performing an action
	Action,

	/// Quitting the game
	Quit,
}

/// Login state
#[derive(Clone, Debug, PartialEq)]
pub enum Login {
	/// Username
	Username,

	/// Unknown user
	CreateUser(String),

	/// New user password
	CreatePassword(String),

	/// New user password again
	CreatePassword2((String, Password)),

	/// Password for existing user
	Password(String),
}
