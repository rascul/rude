use std::collections::{HashMap, VecDeque};

use log::{debug, info};
use mio::{Events, Interest, Poll, Token};

use crate::check;
use crate::client::Client;
use crate::config::Config;
use crate::database::Db;
use crate::game::Game;
use crate::logger;
use crate::result::*;
use crate::server::Server;
use crate::try_log;

impl Game {
	pub fn new() -> RudeResult<Self> {
		let config = Config::load("config.toml")?;
		println!("Loading configuration from config.toml");

		let log_level = config.logging.level.clone();
		try_print(logger::init(log_level), "Unable to initialize logger")?;
		debug!("Initialized logging facility");

		debug!("Setting up check config");
		check::init(&config.player);

		debug!("Opening database");
		let db = Db::open(&config.database)?;

		let events = Events::with_capacity(config.server.event_capacity);
		let clients: HashMap<Token, Client> = HashMap::new();
		let mut tokens: VecDeque<Token> = VecDeque::with_capacity(config.server.connection_limit);

		for i in 1..=config.server.connection_limit {
			tokens.push_back(Token(i));
		}

		let server_address = [&config.server.ip, ":", &config.server.port.to_string()].concat();
		let mut server = Server::listen(&server_address, Token(0))?;
		info!("Listening on {}", &server_address);

		let poll = try_log!(Poll::new(), "Unable to create Poll");
		try_log!(
			poll.registry()
				.register(&mut server.socket, Token(0), Interest::READABLE),
			"Unable to register poll"
		);

		debug!("Accepting connections");

		Ok(Self {
			config,
			server,
			poll,
			events,
			tokens,
			clients,
			db,
		})
	}
}
