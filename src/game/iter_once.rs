use crate::game::Game;
use crate::queue::SendQueue;
use crate::result::RudeResult;
use crate::state::State;
use crate::try_log;

impl Game {
	/// One iteration of the game loop
	pub fn iter_once(&mut self) -> RudeResult<bool> {
		// poll for events
		try_log!(self.poll.poll(&mut self.events, None), "Poll failed");

		// handle socket events and build receive queue
		let mut recv_queue = self.handle_events();

		// get the send queue ready
		let mut send_queue = SendQueue::new();

		// process the receive queue and fill the send queue
		while let Some((token, message)) = recv_queue.pop() {
			let mut queue = self.process_recv_message(token, message);
			send_queue.append(&mut queue);
		}

		// send everything in the send queue
		while let Some((token, message, prompt, state)) = send_queue.pop() {
			// get the client
			let mut client = if let Some(client) = self.clients.remove(&token) {
				client
			} else {
				// no client, so put the token back and move on
				self.tokens.push_back(token);
				continue;
			};

			if prompt {
				client.send_with_prompt(message);
			} else {
				client.send_without_prompt(&message);
			}

			match state {
				Some(State::Quit) => {
					let _ = client.close();
					log::info!("Disconnect from {}", client);
					self.clients.remove(&token);
					let _ = self.db.remove_connected_player(token);
					self.tokens.push_back(token);
				}
				Some(state) => {
					client.state = state;
					self.clients.insert(token, client);
				}
				_ => {
					let _ = self.clients.insert(token, client);
				}
			}
		}

		Ok(true)
	}
}
