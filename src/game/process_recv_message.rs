use log::warn;
use mio::Token;

use crate::game::Game;
use crate::queue::SendQueue;
use crate::state::*;

impl Game {
	/// Process the received message from the client.
	pub fn process_recv_message(&mut self, token: Token, message: String) -> SendQueue {
		let mut send_queue = SendQueue::new();

		let client_state = {
			if let Some(client) = self.clients.get(&token) {
				client.state.clone()
			} else {
				// should probably do something here like give the client a
				// state or close the connection or something?
				warn!("Client has no state: {:?}", token);
				return send_queue;
			}
		};

		match &client_state {
			State::Login(login_state) => {
				let mut queue = self.login(token, message, login_state);
				send_queue.append(&mut queue);
			}
			State::Action | State::Quit => {
				let mut queue = self.action(token, message);
				send_queue.append(&mut queue);
			}
		};

		send_queue
	}
}
