use log::{debug, warn};
use mio::Token;

use crate::command::{Command, Parse};
use crate::game::Game;
use crate::queue::SendQueue;

impl Game {
	/// Figure out the action to be taken and take it.
	pub fn action(&mut self, token: Token, message: String) -> SendQueue {
		let mut send_queue = SendQueue::new();

		// get the player information
		let player = if let Ok(Some(player)) = self.db.get_connected_player(token) {
			player.clone()
		} else {
			warn!("No connected player found: {:?}", &token);
			return SendQueue::error(token);
		};

		// break up the message into lines
		let lines: Vec<String> = message.lines().map(|s| s.into()).collect();

		// no need to do anything else if there's nothing to process
		if lines.is_empty() {
			send_queue.push(token, "", true, None);
			return send_queue;
		}

		// process each line
		for line in lines {
			// parse the command
			let (command, args) = match Command::parse(&line) {
				Ok(c) => c,
				Err(e) => {
					send_queue.push(token, format!("{}", e), true, None);
					continue;
				}
			};

			debug!("{} : {:?}", player.name, command);

			send_queue.append(&mut command.dispatch(&command, args, token, &mut self.db));
		}

		send_queue
	}
}
