use std::net::Shutdown;

use log::{debug, info, warn};

use mio::Interest;

use crate::game::Game;
use crate::queue::RecvQueue;

impl Game {
	pub fn handle_events(&mut self) -> RecvQueue {
		let mut recv_queue = RecvQueue::new();
		let events = self.events.iter().clone();

		for event in events {
			if event.is_read_closed() {
				// remote host has disconnected
				let address: String = match self.clients.remove(&event.token()) {
					Some(client) => client.into(),
					None => "None".into(),
				};
				info!("Disconnect from {}", address);
				let _ = self.db.remove_connected_player(event.token());
				self.tokens.push_back(event.token());
			} else if event.token() == self.server.token {
				// new connection
				if let Some(token) = self.tokens.pop_front() {
					// got a token so haven't reached connection limit
					match self.server.accept(token) {
						Ok(mut client) => {
							match self.poll.registry().register(
								&mut client.socket,
								client.token,
								Interest::READABLE,
							) {
								Ok(_) => {
									// new connection
									info!("Connect from {}", &client);
									recv_queue.push(token, "");
									self.clients.insert(token, client);
								}
								Err(e) => {
									warn!("Register failed: {}: {}", client, e);
									self.tokens.push_back(token);
								}
							};
						}
						Err(e) => {
							warn!("Accept failed: {}", e);
							self.tokens.push_back(token);
						}
					};
				} else {
					// connection limit reached
					warn!("Maximum connections reached");

					let token = event.token();

					// send message and close connection
					if let Ok(mut client) = self.server.accept(token) {
						client.send_without_prompt("Maximum connections reached.\n");
						let _ = client.socket.shutdown(Shutdown::Both);
						info!("Rejected connection from {}", &client);
					}

					// put the token back
					self.tokens.push_back(token);
				}
			} else {
				// there is something to read from a client
				if let Some(client) = self.clients.get_mut(&event.token()) {
					let r = client.read();
					match r {
						Ok(message) => {
							recv_queue.push(event.token(), message);
						}
						Err(e) => debug!("Read from client failed: {}: {}", client, e),
					};
				}
			}
		}

		recv_queue
	}
}
