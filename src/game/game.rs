use std::collections::{HashMap, VecDeque};

use mio::{Events, Poll, Token};

use crate::client::Client;
use crate::config::Config;
use crate::database::Db;
use crate::server::Server;

/// The `Game` struct is everything.
pub struct Game {
	/// Game configuration
	pub config: Config,

	/// Server socket
	pub server: Server,

	/// Poll object
	pub poll: Poll,

	/// Socket events
	pub events: Events,

	/// Available tokens
	pub tokens: VecDeque<Token>,

	/// Connected clients
	pub clients: HashMap<Token, Client>,

	/// Database connection pool
	pub db: Db,
}
