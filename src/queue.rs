//! Queues for recv and send

use std::collections::VecDeque;

use mio::Token;

use crate::state::State;

/// Queue of clients with a message to receive.
///
/// * The client is designated with the [`Token`](../../mio/struct.Token.html).
/// * [`String`](https://doc.rust-lang.org/nightly/alloc/string/struct.String.html)
/// is used to store the message.
#[derive(Debug)]
pub struct RecvQueue(VecDeque<(Token, String)>);

impl RecvQueue {
	/// Create a new, empty `RecvQueue`.
	pub fn new() -> Self {
		RecvQueue(VecDeque::new())
	}

	/// Remove and return the first message in the `RecvQueue`.
	pub fn pop(&mut self) -> Option<(Token, String)> {
		self.0.pop_front()
	}

	/// Add a message to the end of the `RecvQueue`.
	pub fn push<S: Into<String>>(&mut self, token: Token, s: S) {
		self.0.push_back((token, s.into()));
	}
}

/// Queue of messages to send to clients.
///
/// * The client is designated with the [`Token`](../../mio/struct.Token.html).
/// * [`String`](https://doc.rust-lang.org/nightly/alloc/string/struct.String.html)
/// is used to store the message.
/// * [`bool`](https://doc.rust-lang.org/nightly/std/primitive.bool.html) is set to `true` if a
/// prompt is to be displayed following the message.
/// * `Option<State>` to have the player be put into a different play state
#[derive(Debug)]
pub struct SendQueue(pub VecDeque<(Token, String, bool, Option<State>)>);

impl<S: Into<String>> From<(Token, S)> for SendQueue {
	fn from(tuple: (Token, S)) -> Self {
		let (t, s) = tuple;
		Self(vec![(t, s.into(), true, None)].into())
	}
}

impl SendQueue {
	pub fn new() -> Self {
		SendQueue(VecDeque::new())
	}

	pub fn append(&mut self, queue: &mut Self) {
		self.0.append(&mut queue.0);
	}

	pub fn pop(&mut self) -> Option<(Token, String, bool, Option<State>)> {
		self.0.pop_front()
	}

	pub fn push<S: Into<String>>(
		&mut self,
		token: Token,
		s: S,
		prompt: bool,
		state: Option<State>,
	) {
		self.0.push_back((token, s.into(), prompt, state));
	}

	pub fn error(token: Token) -> Self {
		Self(vec![(token, "Error".to_string(), true, None)].into())
	}

	pub fn ok(token: Token) -> Self {
		Self(vec![(token, "Ok".to_string(), true, None)].into())
	}
}
