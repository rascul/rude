//! Player information

use std::cmp::PartialEq;
use std::convert::TryFrom;
use std::error::Error;

use rusqlite::Row;
use serde_derive::{Deserialize, Serialize};

use crate::hash::{hash, salt};
use crate::result::RudeResult;
use crate::try_log;

/// Containing object for the password
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Password {
	/// Salt
	pub salt: String,

	/// Salted and hashed password
	pub hash: String,
}

impl Password {
	/// Create a new password object from a string password.
	pub fn new<S: Into<String>>(s: S) -> RudeResult<Self> {
		let salt = salt();
		let hash = try_log!(hash(s, &salt), "Unable to create hash");

		Ok(Self { salt, hash })
	}

	/// Check the password against the provided password.
	pub fn check<S: Into<String>>(&self, s: S) -> RudeResult<bool> {
		let s = s.into();
		let hash = try_log!(hash(s, &self.salt), "Unable to check hash",);

		Ok(self.hash == hash)
	}
}

impl<'a> TryFrom<&Row<'a>> for Password {
	type Error = Box<dyn Error>;

	fn try_from(row: &Row) -> Result<Self, Self::Error> {
		Ok(Self {
			salt: try_log!(row.get("salt"), "salt"),
			hash: try_log!(row.get("hash"), "hash"),
		})
	}
}

impl PartialEq for Password {
	fn eq(&self, other: &Self) -> bool {
		self.hash == other.hash
	}
}
