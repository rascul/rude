//! Hash and salt functions

use argon2;
use rand::distributions::{Distribution, Uniform};

use crate::result::RudeResult;
use crate::try_log;

/// Compute an argon2 hash
///
/// # Arguments
///
/// * data: string to hash
/// * salt: string to salt the hash with
///
/// # Example
///
/// ```
/// use rude::hash::*;
/// let data = "butterflies";
/// let salt = salt();
/// println!("{}", hash(data, salt).unwrap());
/// ```
pub fn hash<D: Into<String>, S: Into<String>>(data: D, salt: S) -> RudeResult<String> {
	Ok(try_log!(
		argon2::hash_encoded(
			data.into().as_bytes(),
			salt.into().as_bytes(),
			&argon2::Config::default(),
		),
		"Unable to create hash",
	))
}

/// Generate a random salt
///
/// # Example
///
/// ```
/// use rude::hash::salt;
/// let salt = salt();
/// println!("{}", salt);
/// ```
pub fn salt() -> String {
	let between = Uniform::from(32..127);
	let mut rng = rand::thread_rng();
	let mut salt = String::new();

	for _ in 0..64 {
		salt.push(char::from(between.sample(&mut rng)));
	}

	salt
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_hash() {
		let password = "butterflies";
		let salt = "cucumbers";
		let hashed = hash(password, salt).unwrap();

		assert_eq!(
			hashed,
			"$argon2i$v=19$m=4096,t=3,p=1$Y3VjdW1iZXJz$r5IYZSVjTOQoW9bGSv3GseB6pcOSEK8btQ8ftJX5wmE"
		);
	}
}
