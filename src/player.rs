//! Player information

use std::convert::TryFrom;
use std::error::Error;

use chrono::{DateTime, Utc};
use rusqlite::Row;
use serde_derive::{Deserialize, Serialize};

use crate::id::Id;

/// Player information
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Player {
	/// Unique identifier
	pub id: Id,

	/// Player name
	pub name: String,

	/// Creation DateTime
	pub created: DateTime<Utc>,

	/// Player's location
	pub location: Id,
}

impl<'a> TryFrom<&Row<'a>> for Player {
	type Error = Box<dyn Error>;

	fn try_from(row: &Row) -> Result<Self, Self::Error> {
		Ok(Self {
			id: try_log!(row.get("id"), "id"),
			name: try_log!(row.get("name"), "name"),
			created: try_log!(row.get("created"), "created"),
			location: try_log!(row.get("location"), "location"),
		})
	}
}
