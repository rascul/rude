/// Log a message to the error log and add file and line.
#[macro_export]
macro_rules! log_error {
	($($arg:tt)*) => {
		let s = std::fmt::format(format_args!($($arg)*));
		log::error!("{}({}) :: {}", file!(), line!(), s);
	}
}

/// Unwrap a Result. If it's Err then log the error and provided format string
/// and return.
///
/// # Arguments
///
/// * `$e:expr` : Expression to evaluate, must return `Result<T, Box<dyn Error>>`
/// * `$($arg:tt)*` : Format string and parameters (like `println!()`)
///
/// # Example
///
/// ```
/// use std::fs::File;
/// use rude::result::RudeResult;
/// use rude::try_log;
///
/// fn main() -> RudeResult<()> {
///     let file = try_log!(File::open(file!()), "Unable to open {}", file!());
///     Ok(())
/// }
/// ```
#[macro_export]
macro_rules! try_log {
	($e:expr, $($arg:tt)*) => {
		match $e {
			Ok(r) => r,
			Err(e) => {
				let s = std::fmt::format(format_args!($($arg)*));
				log::error!("{}({}) :: {} :: {}", file!(), line!(), s, e);
				return Err(Box::from(e));
				}
			}
	};
}

/// Unwrap a Result. If it's Err then print the error and provided format string
/// and return.
///
/// # Arguments
///
/// * `$e:expr` : Expression to evaluate, must return `Result<T, Box<dyn Error>>`
/// * `$($arg:tt)*` : Format string and parameters (like `println!()`)
///
/// # Example
///
/// ```
/// use std::fs::File;
/// use rude::result::RudeResult;
/// use rude::try_print;
///
/// fn main() -> RudeResult<()> {
///     let file = try_print!(File::open(file!()), "Unable to open {}", file!());
///     Ok(())
/// }
/// ```
#[macro_export]
macro_rules! try_print {
	($e:expr, $($arg:tt)*) => {
		match $e {
			Ok(r) => r,
			Err(e) => {
				let s = std::fmt::format(format_args!($($arg)*));
				println!("{}({}) :: {} :: {}", file!(), line!(), s, e);
				return Err(Box::from(e));
				}
			}
	};
}

/// Unwrap a Result. If it's Err then print the error and return a generic
/// error message for the client.
///
/// # Arguments
///
/// * `$i:ident` : mio::Token to designate the client
/// * `$e:expr` : Expression to evaluate, must return `Result<T, Box<dyn Error>>`
///
/// # Example
///
/// ```
/// use std::fs::File;
/// use mio::Token;
/// use rude::queue::SendQueue;
/// use rude::try_send_error;
///
/// fn check_file() -> SendQueue {
///     let token = Token(12);
///     let file = try_send_error!(token, File::open(file!()));
///     SendQueue::ok(token)
/// }
///
/// # assert_eq!(check_file().0[0].1, SendQueue::ok(Token(12)).0[0].1);
/// ```
#[macro_export]
macro_rules! try_send_error {
	($i:ident, $e:expr) => {
		match $e {
			Ok(r) => r,
			Err(e) => {
				log::error!(
					"{}({}) :: returning SendQueue::error() :: {}",
					file!(),
					line!(),
					e
				);
				return $crate::queue::SendQueue::error($i);
				}
			}
	};
}

/// Unwrap a `Result` and return a custom message to send to the client for Err.
///
/// # Arguments
///
/// * `$i:ident` : `mio::Token` client identifier
/// * `$e:expr` : Expression to evaluate, must return `Result<T, Box<dyn Error>>`
/// * `$($arg:tt)*` : Format string and parameters (like `println!()`)
///
/// # Example
///
/// ```
/// use std::fs::File;
/// use mio::Token;
/// use rude::queue::SendQueue;
/// use rude::try_send;
///
/// fn check_file() -> SendQueue {
///     let token = Token(12);
///     let file = try_send!(token, File::open(file!()), "Unable to open {}", file!());
///     SendQueue::ok(token)
/// }
///
/// # assert_eq!(check_file().0[0].1, SendQueue::ok(Token(12)).0[0].1);
/// ```
#[macro_export]
macro_rules! try_send {
	($i:ident, $e:expr, $($arg:tt)*) => {
		match $e {
			Ok(r) => r,
			Err(_e) => {
				return $crate::queue::SendQueue(
					vec![($i, std::fmt::format(format_args!($($arg)*)), true, None)].into()
				);
			}
		}
	};
}

/// Unarap an `Option` and return a custom message for the client if it is None.
///
/// # Arguments
///
/// * `$i:ident` : `mio::Token` client identifier
/// * `$e:expr` : Expression to evaluate, must return `Option<T>`
/// * `$($arg:tt)*` : Format string and parameters (like `println!()`)
///
/// # Example
///
/// ```
/// use mio::Token;
/// use rude::queue::SendQueue;
/// use rude::option_send;
///
/// fn check_file() -> SendQueue {
///     let token = Token(12);
///     let option = Some(4);
///     let send = option_send!(token, option, "Option unwrapped to None");
///     SendQueue::ok(token)
/// }
///
/// # assert_eq!(check_file().0[0].1, SendQueue::ok(Token(12)).0[0].1);
/// ```
#[macro_export]
macro_rules! option_send {
	($i:ident, $e:expr, $($arg:tt)*) => {
		match $e {
			Some(r) => r,
			None => {
				return $crate::queue::SendQueue(
					vec![($i, std::fmt::format(format_args!($($arg)*)), true, None)].into()
				);
			}
		}
	};
}

/// Unwrap a `Result<Option<T>, E>` and for Err or None, log a message and send
/// a generic error message to the client.
///
/// # Arguments
///
/// * `$i:ident` : `mio::Token` client identifier
/// * `$e:expr` : Expression to evaluate, must return `Result<T, Box<dyn Error>>`
///
/// # Example
///
/// ```
/// use std::error::Error;
/// use mio::Token;
/// use rude::queue::SendQueue;
/// use rude::try_option_send_error;
///
/// fn tor() -> SendQueue {
///     let token = Token(12);
///     let tor: Result<Option<usize>, Box<dyn Error>> = Ok(Some(42));
///     let file = try_option_send_error!(token, tor);
///     SendQueue::ok(token)
/// }
///
/// # assert_eq!(tor().0[0].1, SendQueue::ok(Token(12)).0[0].1);
/// ```
#[macro_export]
macro_rules! try_option_send_error {
	($i:ident, $e:expr) => {
		match $e {
			Ok(Some(r)) => r,
			Ok(None) => {
				log::error!(
					"{}({}) :: returning SendQueue::error() :: None value",
					file!(),
					line!()
				);
				return $crate::queue::SendQueue::error($i);
				}
			Err(e) => {
				log::error!(
					"{}({}) :: returning SendQueue::error() :: {}",
					file!(),
					line!(),
					e
				);
				return $crate::queue::SendQueue::error($i);
				}
			}
	};
}
