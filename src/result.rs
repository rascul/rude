use std::error::Error;

use log::error;

/// Wrapper for `Result<T, Box<dyn Error>>`.
///
/// # Example
/// ```
/// use rude::result::RudeResult;
///
/// fn example_result() -> RudeResult<()> {
///     Ok(())
/// }
/// ```
pub type RudeResult<T> = Result<T, Box<dyn Error>>;

/// Wrap `?` with an error message to stdout.
pub fn try_print<T, E: Into<Box<dyn Error>>, S: Into<String>>(
	result: Result<T, E>,
	message: S,
) -> RudeResult<T> {
	try_error(result, message, false)
}

/// Wrap `?` with an error message to either stdout or the log.
///
/// * `result` - `Result` type to check for `Error`.
/// * `message` - Message to send to either log or stdout if `result` is `Error`.
/// * `log` - `true` to send `message` to log, otherwise send to stdout.
//pub fn try_error<T, S: Into<String>>(result: Result<T>, message: S, log: bool) -> Result<T> {
pub fn try_error<T, E: Into<Box<dyn Error>>, S: Into<String>>(
	result: Result<T, E>,
	message: S,
	log: bool,
) -> RudeResult<T> {
	match result {
		Ok(r) => Ok(r),
		Err(e) => {
			let e = e.into();
			if log {
				error!("{} :: {}", message.into(), e);
			} else {
				println!("{} :: {}", message.into(), e);
			}
			Err(e.into())
		}
	}
}
