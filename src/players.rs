//! All the players of the game

use std::collections::HashMap;

use uuid::Uuid;

use crate::player::Player;

/// Map of each player id to [`Player`](../Player/struct.Player.html) object.
pub type Players = HashMap<Uuid, Player>;

/// Methods for the [`Players`](type.Players.html) type
pub trait PlayersMethods {
	fn find_by_name<S: Into<String>>(&self, name: S) -> Option<Player>;
}

impl PlayersMethods for Players {
	/// Find a [`Player`](../Player/struct.Player.html) by the player name.
	fn find_by_name<S: Into<String>>(&self, name: S) -> Option<Player> {
		let name = name.into();
		match self.iter().find(|(_id, player)| player.name == name) {
			Some((_id, player)) => Some(player.clone()),
			None => None,
		}
	}
}
