mod check;
mod client;
mod command;
mod config;
mod database;
mod game;
mod hash;
mod id;
mod logger;
#[macro_use]
mod macros;
mod password;
mod player;
mod queue;
mod result;
mod server;
mod state;
mod world;

use log::*;

use crate::game::Game;
use crate::result::RudeResult;

fn main() -> RudeResult<()> {
	let mut game = Game::new()?;

	loop {
		match game.iter_once() {
			Ok(true) => continue,
			Ok(false) => break,
			Err(e) => return Err(e),
		}
	}

	Ok(())
}
