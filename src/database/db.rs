use std::path::Path;

use mio::Token;
use rusqlite::{params, Connection};

use crate::id::Id;
use crate::player::Player;
use crate::result::RudeResult;
use crate::try_log;

/// Wrapper around the database connection
pub struct Db(pub Connection);

impl Db {
	/// Open the database
	pub fn open<P: AsRef<Path>>(path: P) -> RudeResult<Self> {
		// open the database
		let connection = try_log!(Connection::open(path), "Unable to open database");

		// make sure the connected_players table is cleared out
		{
			let mut statement = try_log!(
				connection.prepare("delete from connected_players;"),
				"Unable to prepare sql statement"
			);

			let _ = try_log!(
				statement.execute(params![]),
				"Unable to execute sql statement"
			);
		}

		Ok(Self(connection))
	}

	pub fn single_save_player(&self, token: Token, player: &Player) -> RudeResult<()> {
		let _ = self.save_player(&player)?;
		self.save_connected_player(token, &player)
	}

	/// Get a new player id, checked to ensure uniqueness.
	pub fn new_player_id(&self) -> RudeResult<Id> {
		let mut statement = try_log!(
			self.0.prepare("select * from players where id=?;"),
			"Unable to prepare sql statement"
		);

		loop {
			let id = Id::new();
			let mut rows = try_log!(statement.query(params![id]), "Unable to perform query");
			if try_log!(rows.next(), "Unable to retrieve row").is_none() {
				return Ok(id);
			}
		}
	}

	/// Get a new area id, checked to ensure uniqueness.
	pub fn new_area_id(&self) -> RudeResult<Id> {
		let mut zones_statement = try_log!(
			self.0.prepare("select * from zones where id = ?;"),
			"Unable to prepare sql statement"
		);

		let mut rooms_statement = try_log!(
			self.0.prepare("select * from rooms where id = ?;"),
			"Unable to prepare sql statement"
		);

		loop {
			let id = Id::new();

			let mut rows = try_log!(
				zones_statement.query(params![id]),
				"Unable to perform query"
			);
			if try_log!(rows.next(), "Unable to retrieve row").is_none() {
				let mut rows = try_log!(
					rooms_statement.query(params![id]),
					"Unable to perform sql query"
				);
				if try_log!(rows.next(), "Unable to retrieve row").is_none() {
					return Ok(id);
				}
			}
		}
	}
}
