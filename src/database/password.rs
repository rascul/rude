use std::convert::TryFrom;

use rusqlite::params;

use crate::database::Db;
use crate::id::Id;
use crate::password::Password;
use crate::result::RudeResult;
use crate::try_log;

impl Db {
	/// Get a password
	pub fn get_password(&self, id: Id) -> RudeResult<Option<Password>> {
		let mut statement = try_log!(
			self.0
				.prepare("select salt, hash from passwords where player = ?"),
			"Unable to prepare sql statement"
		);

		let mut rows = try_log!(statement.query(params![id]), "Unable to perform query");

		if let Some(row) = try_log!(rows.next(), "Unable to retrieve row") {
			Ok(Some(try_log!(
				Password::try_from(row),
				"Unable to get Password from Row"
			)))
		} else {
			Ok(None)
		}
	}

	/// Save a password
	pub fn save_password(&self, id: Id, password: &Password) -> RudeResult<()> {
		let mut statement = try_log!(
			self.0.prepare(
				"insert into passwords (player, salt, hash) values (?, ?, ?) on conflict(player) do update set player=?, salt=?, hash=?;"
			),
			"Unable to prepare statement"
		);

		let _ = try_log!(
			statement.execute(params![
				id,
				password.salt,
				password.hash,
				id,
				password.salt,
				password.hash,
			]),
			"Unable to perform query"
		);

		Ok(())
	}
}
