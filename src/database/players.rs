use std::convert::TryFrom;

use rusqlite::params;

use crate::database::Db;
use crate::id::Id;
use crate::player::Player;
use crate::result::RudeResult;
use crate::try_log;

impl Db {
	/// Load a player from the database.
	pub fn _load_player(&self, id: Id) -> RudeResult<Option<Player>> {
		let mut statement = try_log!(
			self.0
				.prepare("select id, name, created, location from players where id = ?"),
			"Unable to prepare sql statement"
		);

		let mut rows = try_log!(statement.query(params![id]), "Unable to perform query");

		Ok(
			if let Some(row) = try_log!(rows.next(), "Unable to retrieve row") {
				Some(try_log!(
					Player::try_from(row),
					"Unable to get Player from Row"
				))
			} else {
				None
			},
		)
	}

	/// Find a player by the name
	pub fn find_player_by_name<S: Into<String>>(&self, name: S) -> RudeResult<Option<Player>> {
		let mut statement = try_log!(
			self.0
				.prepare("select id, name, created, location from players where name = ?"),
			"Unable to prepare sql statement"
		);

		let mut rows = try_log!(
			statement.query(params![name.into()]),
			"Unable to perform query"
		);

		Ok(
			if let Some(row) = try_log!(rows.next(), "Unable to retrieve row") {
				Some(try_log!(
					Player::try_from(row),
					"Unable to get Player from Row"
				))
			} else {
				None
			},
		)
	}

	/// Save a player to the database.
	pub fn save_player(&self, player: &Player) -> RudeResult<()> {
		let mut statement = try_log!(
			self.0.prepare(
				"insert into players (id, name, created, location) values (?, ?, ?, ?) on conflict(id) do update set id=?, name=?, created=?, location=?;"
			),
			"Unable to prepare statement"
		);

		let _ = try_log!(
			statement.execute(params![
				player.id,
				player.name,
				player.created,
				player.location,
				player.id,
				player.name,
				player.created,
				player.location,
			]),
			"Unable to perform query"
		);

		Ok(())
	}
}
