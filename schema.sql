-- sqlite

create table zones (
    id text not null primary key,
    parent text not null,
    name text,
    users_visible numeric not null,
    foreign key(parent) references zones(id)
);

insert into zones (id, parent, name, users_visible) values (
    '3a5f959c-bf2d-4526-a641-d3d3bb07c1d7',
    '3a5f959c-bf2d-4526-a641-d3d3bb07c1d7',
    'City',
    1
);

create table rooms (
    id text not null primary key,
    zone text not null,
    name text,
    description text,
    users_visible numeric not null,
    foreign key(zone) references zones(id)
);

insert into rooms (id, zone, name, description, users_visible) values (
    'd027ff76-185f-4da0-941c-7cadc6cf6e20',
    '3a5f959c-bf2d-4526-a641-d3d3bb07c1d7',
    'Town Square',
    'The central square of the town.',
    1
);

create table exits (
    room text not null,
    target text not null,
    direction text not null,
    foreign key(room) references rooms(id),
    foreign key(target) references rooms(id)
);

create table players (
    id text not null primary key,
    name text not null unique,
    created numeric not null,
    location text not null,
    foreign key(location) references rooms(id)
);

create table connected_players (
    token integer not null primary key,
    player text not null unique,
    foreign key(player) references players(id)
);

create table passwords (
    player text not null primary key,
    salt text not null,
    hash text not null,
    foreign key(player) references players(id)
);
